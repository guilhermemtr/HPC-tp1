#include <iostream>
#include <vector>
#include "statistics.h"
#include "logger.h"
#include "forest.h"
#include "time_lib.h"
#include "gnuplot-iostream.h"

//default size for the problem
#define ROWS (1 << 11)
#define COLS (1 << 11)

#define NUM_TESTS 100
#define DISCARD 0.10

#define STATE_LOG_STEP 1
#define STATE_LOG_NAME "states/forest"

void benchmark(int prob, long x, long y,
               long tc, long tr, long cols, 
               long rows) {
  srand(time(NULL));

  statistics stats(NUM_TESTS,DISCARD);
  int i;
  for(i = 0; i < NUM_TESTS; i++) {
    printf("starting benchmark %d\n", i+1);
    forest*  f = new forest(cols, rows, x, y, prob);
    
    time_lib chr;
    __log("main:Starting test (%d)\n", i);
    chr.start();
    if(tc > 0 && tr > 0)
      f->burn(tc, tr);
    else
      f->burn();
    chr.finish();
    __log("main:Ending test (%d)\n", i);

    stats.add_value(chr.get_time());

   
    delete f;
  }

  printf("Average time:\t\t%lf seconds\nStandard Deviation:\t%lf seconds\n", 
    stats.get_avg(), stats.get_std_dev());
}


void test_fire_spread(int prob, long x, long y,
               long tc, long tr, long cols, 
               long rows) {
  Gnuplot gp_area;
  Gnuplot gp_steps;
  gp_area << "set xrange [0:100]\nset yrange [0:1]\n";
  gp_steps << "set xrange [0:1]\nset yrange [0:" << cols + rows << "]\n";
  std::vector<std::pair<int,double>> areas;
  std::vector<std::pair<int,long>> steps;
  srand(time(NULL));


  while(prob <= 100) {
    printf("running for probability %d\n", prob);
    int i;
    for(i = 0; i < NUM_TESTS; i++) {
      forest*  f = new forest(cols, rows, x, y, prob);
    
      if(tc > 0 && tr > 0)
        f->burn(tc, tr);
      else
        f->burn();

      areas.push_back(std::make_pair(prob, f->get_burnt_area()));
      steps.push_back(std::make_pair(prob, f->get_time_step()));
      delete f;
    }
    prob++;
  }
  gp_area << "plot" << gp_area.file1d(areas) 
      << "with points title 'area'" << std::endl;
  //gp_steps << "plot" << gp_steps.file1d(steps) 
  //    << "with points title 'circle'" << std::endl;

}


void example_fire(int prob, long x, long y,
               long tc, long tr, long cols, 
               long rows) {

  srand(time(NULL));
  int   time_step;
  float burnt_area;
  forest*  f = new forest(cols, rows, x, y, prob);
  f->save_states(STATE_LOG_NAME, STATE_LOG_STEP);
  if(tc > 0 && tr > 0)
    f->burn(tc, tr);
  else
    f->burn();
  time_step  = f->get_time_step();
  burnt_area = f->get_burnt_area();
  delete f;

  printf("The simulation ran for %d generations.\n"     , time_step );
  printf("The percentage of the forest burned was %f\n" , burnt_area);
}

int main(int argc, char * argv[]) {
  long cols = COLS;
  long rows = ROWS;

  if (argc < 6){
    printf("Usage:\n%s <probability> <x> <y> <thread_columns or 0> "
           "<thread_rows or 0> <columns(optional)> <rows(optional)>\n", argv[0]);
    return 1;
  }

  int  prob = atoi(argv[1]);
  long x    = atol(argv[2]);
  long y    = atol(argv[3]);
  long tc   = atol(argv[4]);
  long tr   = atol(argv[5]);

  //custom sized forest
  if (argc == 8) {
    cols = atol(argv[6]);
    rows = atol(argv[7]);
  }

  printf("Forest Fire Demonstration Program\n"                    );
  printf("Forest size is [%ld][%ld]\n"                , cols, rows);
  printf("Probability:  %d\nStart at grid[%ld][%ld]\n", prob, x, y);
  

  //benchmark(prob, x, y, tc, tr, cols, rows);
  //test_fire_spread(prob, x, y, tc, tr, cols, rows);
  example_fire(prob, x, y, tc, tr, cols, rows);



  return 0;

}
