
// In order to use the the logger, you have to define __YAL_ON__ flag.
// If the flag is not defined, there will be no logs.

#ifndef __YAL_LOG__
#define __YAL_LOG__

#include <stdio.h>

#ifdef __YAL_ON__
#define __log(...) do {		\
    printf(__VA_ARGS__);		\
  } while(0)
#else
#define __log(...) {}
#endif

#endif
