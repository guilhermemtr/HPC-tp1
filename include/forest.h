#include <unordered_map>
#include <sstream>

#include "utils.h"
#include "grid.h"
#include "logger.h"
#include "ppm.h"

#define GRAIN_SIZE 4

//256 MB as maximum ram storage for forest history
#define SZ_THRESH (1 << 28)

#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef FOREST_H
#define FOREST_H

class forest
{
  private:
    //the grids
    grid *old, *curr;

    //number of columns, rows
    //first burning point
    //current time
    long cols, rows, x, y, time;

    //probability to burn
    int prob;

    //history of forest state
    std::unordered_map<long,grid*> forest_history;

    //save the states to a file?
    bool save_states_to_file;

    //filename for saving the states
    std::string filename;

    //save_step
    long save_step;

  public:
    /**
     * Default constructor
     */
    forest(long cols, long rows, long x, long y, int prob);

    /**
     * burns the forest until it does not burn anymore.
     * not thread safe
     */
    void burn();

    /**
     * burns the forest until it does not burn anymore.
     * work division is made given a number of rows and 
     *   columns in which each thread will work on.
     * not thread safe
     */
    void burn(long tc, long tr);

    /**
     * returns the burnt fire area.
     * not thread safe when burn is executing.
     */
    float get_burnt_area();

    /**
     * returns the current burning area
     * not thread safe when burn is executing.
     */
    float get_burning_area();

    /**
     * returns the total burn area
     * not thread safe when burn is executing.
     */
    float get_fire_area();

    /**
     * returns the current time step
     * may return values for unfinished steps
     * (maximum difference is 1)
     */
    long get_time_step();

    /**
     * given a time step, and if the executions are set to be
     *  saved, it returns the state of the grid at the time passed
     *  as the parameter.
     * not thread safe when burn is executing.
     */
    grid* get_step(long time);

    /**
     * given a filename, it marks executions to be saved to files.
     *  each timestep will correspond to a file.
     *  this method must be called before runing burn in order to
     *  save the execution states.
     * thread safe, but unknown chosenfilename upon parallel execution
     */
    void save_states(std::string file, long save_steps = 1);

    /**
     * flushes the current history to files.
     *   useful when running out of memory
     */
    void flush();

    /**
     * Default destructor
     * if save states is set, it saves the states of the execution
     *  and it also frees any allocated resources
     */
    virtual ~forest();
};

#endif // FOREST_H
