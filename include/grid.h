#include <cstdlib>
#include <iostream>
#include <sstream>
#include "utils.h"
#include "logger.h"

#ifdef _OPENMP
#include <omp.h>
#endif

#ifndef GRID_H
#define GRID_H

class grid
{
  friend class forest;
  friend class file2ppm;
  public:
    /** Default constructor */
    grid(long cols, long rows);
    grid(grid& o);

    void update(grid* old);

    inline int get_val_neighbors(int val, long x, long y) {
      return
        mat[get_pos(x-1,y-1)] == val  ||
        mat[get_pos(x-1,y  )] == val  ||
        mat[get_pos(x-1,y+1)] == val  ||
        mat[get_pos(x  ,y-1)] == val  ||
        mat[get_pos(x  ,y+1)] == val  ||
        mat[get_pos(x+1,y-1)] == val  ||
        mat[get_pos(x+1,y  )] == val  ||
        mat[get_pos(x+1,y+1)] == val;
    }

    inline float get_area(int val) {
      long x, y, ctr = 0;
      #pragma omp parallel for num_threads(num_cpus()) \
        private(x) reduction(+:ctr)
      for(y = 0; y < rows; y++) {
        for(x = 0; x < cols; x++) {
          if(mat[get_pos(x,y)] == val)
            ctr++;
        }
      }
      return ((float)ctr)/((float)((cols-2)*(rows-2)));
    }

    /** Default destructor */
    virtual ~grid();
    
  protected:
    inline long get_pos(long x, long y) {
        return x + y * cols;
    }

    inline void set_mat(unsigned char* mat) {
      this->mat = mat;
    }

    long cols, rows;
    unsigned char* mat;
};

#endif // GRID_H
