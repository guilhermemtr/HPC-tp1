#include "ppm.h"

file2ppm::file2ppm(long cols, long rows, unsigned char* mat) {
	this->mat  = mat ;
	this->cols = cols;
	this->rows = rows;
}

file2ppm::file2ppm(grid& g) {
	mat = g.mat;
	cols = g.cols;
	rows = g.rows;
}

file2ppm::file2ppm(char* name, long cols, long rows) {
	int l;
  FILE *fint;
  l = cols * rows;
  mat = (unsigned char *) calloc(l, sizeof(unsigned char));
  mat_own = true;

  fint = fopen(name, "r");
  fread(mat, sizeof(unsigned char), l, fint);
  this->cols = cols;
  this->rows = rows;
}

void file2ppm::save_grid(std::string file) {
	save_grid(file.c_str());
}

void file2ppm::save_grid(char* name) {
	FILE * fp;
  unsigned char *image;
  int i, j;
  unsigned char *apt;
  image = (unsigned char *) calloc(cols*rows*3, sizeof(unsigned char));
  fp = fopen(name, "w");
  /* Print the P6 format header */
  fprintf(fp, "P6\n%ld %ld\n255\n", cols, rows);
  apt = mat;
  //parallelize
  #pragma omp parallel for num_threads(num_cpus()) \
    private(j,apt)
  for (i=0; i<rows; i++) {
    apt = mat + i*cols;
    for (j=0; j<cols; j++) {
    	image[(cols*i+j)*3+0] = forest_colors[*apt][0];
    	image[(cols*i+j)*3+1] = forest_colors[*apt][1];
    	image[(cols*i+j)*3+2] = forest_colors[*apt][2];
      apt++;
    }
  }
  fwrite(image, sizeof(unsigned char), cols*rows*3, fp);
  fclose(fp);
  free(image);
}

file2ppm::~file2ppm() {
	if(mat_own) {
		free(mat);
	}
}
