#include "statistics.h"

static int
compare_double(const void * a, const void * b)
{
  double x1 = *(float *) a;
  double x2 = *(float *) b;
  if (x1 == x2)
    return 0;
  if (x1 < x2)
    return -1;
  else
    return 1;
}

statistics::statistics(int n_tests)
{
    this->n_tests = n_tests;
    ctr = 0;
    times = (double *) malloc(n_tests * sizeof(double));
    to_discard = 0.0f;
}

statistics::statistics(int n_tests, double to_discard)
{
    this->n_tests = n_tests;
    ctr = 0;
    times = (double *) malloc(n_tests * sizeof(double));
    to_discard = to_discard;
}

void statistics::add_value(double value)
{
    if(ctr < n_tests)
        times[ctr++] = value;
    else
        throw ctr;
    if(ctr == n_tests)
        qsort(times,n_tests,sizeof(double),compare_double);
}

double statistics::get_avg()
{
    if(ctr != n_tests)
        throw ctr;
    double sum = 0.0f;
    int start = n_tests * to_discard;
    int end = n_tests - start;
    for(int i = start; i < end; i++) {
        sum += times[i];
    }
    return sum/((double)end - start);
}

double statistics::get_std_dev()
{
    if(ctr != n_tests)
        throw ctr;
    int start = n_tests * to_discard;
    int end = n_tests - start;

    double average = get_avg();

    double variance = 0.0;
    for (int i = start; i < end; i++)
    {
      variance = variance + pow(times[i] - average, 2.0f);
    }
    variance = variance / ((double) (end - start) - 1.0f);
    return pow(variance,0.5f);
}

statistics::~statistics()
{
    free(times);
}
