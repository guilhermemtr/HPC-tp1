#include "time_lib.h"

time_lib::time_lib()
{
}

void time_lib::start()
{
        this->begin = std::chrono::system_clock::now();
}

void time_lib::finish()
{
        this->end = std::chrono::system_clock::now();
}

double time_lib::get_time()
{
    std::chrono::duration<double> seconds = this->end - this->begin;
    return seconds.count();
}

time_lib::~time_lib()
{
}
